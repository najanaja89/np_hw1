﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Text.RegularExpressions;

namespace np_hw1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //if (Regex.IsMatch(textBoxHostName.Text, @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b"))
            //{

            //}
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var hostName = textBoxHostName.Text;
                var hostInfo = Dns.GetHostEntry(hostName);
                //var infoList = new List<char>();
                //infoList.AddRange(hostInfo.AddressList.ToString().ToList());
                textBlockInfo.Items.Add(hostInfo.HostName);
                foreach (var alias in hostInfo.Aliases)
                {
                    textBlockInfo.Items.Add(alias);
                }
                foreach (var ip in hostInfo.AddressList)
                {
                    textBlockInfo.Items.Add(ip);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}

